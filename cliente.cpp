#include "cliente.hpp"
#include <iostream>

Cliente::Cliente(){
    set_nome("");
    set_idade(0);
    set_cpf(0);
    set_email("email.com");
    std::cout << "Construtor da classe Cliente" << std::endl; 
}
Cliente::~Cliente(){
    std::cout << "Destrutor da classe Cliente"<< std::endl;
}

void Cliente::set_nome(string nome){
    this->nome = nome;
}
string Cliente::get_nome(){
    return nome;
}
void Cliente::set_idade(int idade){
    if(idade < 0)
        throw 1;
    this->idade = idade;
}
int Cliente::get_idade(){
    return idade;
}
void Cliente::set_cpf(long int cpf){
    this->cpf = to_string(cpf);
}
long int Cliente::get_cpf(){
    return stoi(cpf);
}
void Cliente::set_email(string email){
    this->email = email;
}
string Cliente::get_email(){
    return email;
}
void Cliente::imprime_dados(){
    std::cout << "Nome: " << nome << std::endl;
    std::cout << "Idade: " << idade << std::endl;
    std::cout << "CPF: " << cpf << std::endl;
    std::cout << "Email: " << email << std::endl;
} 






