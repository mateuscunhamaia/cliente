#include "pessoa.hpp"
#include <iostream>

using namespace std;

int main(int argc, char ** argv){

    Pessoa pessoa1;

    pessoa1.set_nome("João");
    pessoa1.set_cpf(23423423423);
    pessoa1.set_telefone("555-444");
    pessoa1.set_email("joao@email.com");

    cout << "Nome: " << pessoa1.get_nome() << endl;
    cout << "CPF: " << pessoa1.get_cpf() << endl;
    cout << "Telefone: " << pessoa1.get_telefone() << endl;
    cout << "Email: " << pessoa1.get_email() << endl;

    return 0;
}




